%script to find plotting 2 best significant features (scatter plot) by performing correlation analysis
%written by Md Nasir (mdnasir@usc.edu)

% static functionals, dynamic functionals
% behavioral codes not included

clear; clc; close all;

run ../utils/figure_configuration.m
run ../utils/add_path.m

load('~/a1/outcome_paper/src/all_acoustic.mat')

final_y(find(final_y == 3 | final_y == 2 | final_y == 1 )) = 0;
final_y(find(final_y == 4)) = 1;

X = final_X;
y = final_y;

clear final_X final_y

% impute NaN
for col = 1:size(X, 2)
    row = find(isnan(X(:, col)) == 1);
    X(row, col) = nanmean(X(:, col));
end


% normalization
X_mean = mean(X, 1);
X_std = nanstd(X);

for col = 1:size(X,2)
    if X_std(col) > 0
        X_norm(:, col) = (X(:, col) - repmat(X_mean(col), size(X, 1), 1))./repmat(X_std(col), size(X, 1), 1);
    else
        X_norm(:, col) = X(:, col) - repmat(X_mean(col), size(X, 1), 1);
    end
end

[r, p]=corr(X,y);
r(isnan(r))=0;
inn=find(abs(r) >=0.2);

[rr, ii]=sort(abs(r),'descend');

%important features
RR=[rr(1:50) ii(1:50)];


fid1=3916; fid2=5545;
xx=X_norm(:,fid1);
xx0=xx(find(y==0));
xx1=xx(find(y==1));

xy=X_norm(:,fid2);
xy0=xy(find(y==0));
xy1=xy(find(y==1));
s1=scatter(xx0,xy0);

s1.MarkerEdgeColor = 'r';
s1.MarkerFaceColor = 'r';

hold on
s2=scatter(xx1,xy1);
s2.MarkerEdgeColor = 'b';
s2.MarkerFaceColor = 'b';

legend('class 0','class 1')
xlabel('loudness feature(normalized)')
ylabel('pitch delta feature(normalized)')


    
saveas(gca,'../analysis/scatter_2feat.fig')
saveas(gca,'../analysis/scatter_2feat','epsc')
saveas(gca,'../analysis/scatter_2feat.pdf')