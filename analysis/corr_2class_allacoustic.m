%script to find significant features by doing correlation analysis
%written by Md Nasir (mdnasir@usc.edu)

% static functionals, dynamic functionals
% behavioral codes not included



clear; clc; close all;


run ../utils/figure_configuration.m
run ../utils/add_path.m

%load feature file
load('~/a1/outcome_paper/src/all_acoustic.mat')



final_y(find(final_y == 3 | final_y == 2 | final_y == 1 )) = 0;
final_y(find(final_y == 4)) = 1;

X = final_X;
y = final_y;

clear final_X final_y

% impute NaN
for col = 1:size(X, 2)
    row = find(isnan(X(:, col)) == 1);
    X(row, col) = nanmean(X(:, col));
end


% normalization
X_mean = mean(X, 1);
X_std = nanstd(X);

for col = 1:size(X,2)
    if X_std(col) > 0
        X_norm(:, col) = (X(:, col) - repmat(X_mean(col), size(X, 1), 1))./repmat(X_std(col), size(X, 1), 1);
    else
        X_norm(:, col) = X(:, col) - repmat(X_mean(col), size(X, 1), 1);
    end
end

[r, p]=corr(X,y);
r(isnan(r))=0;
inn=find(abs(r) >=0.2);



[rr, ii]=sort(abs(r),'descend');


%important features
RR=[rr(1:50) ii(1:50)]


fid=3916; 
res=10;
 xx=X_norm(:,fid);
 xx0=xx(find(y==0));
 xx1=xx(find(y==1));
 h0=histogram(xx0,res);
 hold on
 h1=histogram(xx1,res);

 
     
    legend('class 0','class 1')
    xlabel('loudness feature(normalized)')
    %ylabel('')
    %axis([1 N 0 0.055])

annotation('textbox',...
    [0.181357142857143 0.597619047619048 0.316857142857143 0.0690476190476195],...
    'String',{'Correlation: 0.2924'},...
    'FitBoxToText','on');

    
 %   saveas(gca,'../analysis/loud_hist.fig')
 %       saveas(gca,'../analysis/loud_hist.eps','epsc')
 %       saveas(gca,'../analysis/loud_hist.pdf')
