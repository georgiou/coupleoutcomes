Software supporting "Predicting Couple Therapy Outcomes Based on Speech Acoustic Features"
------------------------------------------------------------------------------------------
written by Md Nasir
-------------------
Updated on September 21, 2016
------------------------

Scripts are written in Matlab and Python
----------------------------------------
Tested in MATLAB R2016a and Python 2.7 respectively

------------
Dependencies
------------
1. Bash
2. Perl
3. Praat
3. OpenSMILE
4. FEAST toolbox used for feature selection
	http://mloss.org/software/view/386/


------------------------
Summary of directories and files
--------------------------------

feat/ :- Directory containing scripts for acoustic feature extraction and functional computation

classify/ :- Directory containing scripts for classification experiments

analysis/ :- Directory containing scripts for correlation analysis

utils/ :- Directory containing utility files used by other scripts

			