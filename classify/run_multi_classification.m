% This script runs binary classification for outcome prediction

run configuration.m
run add_path.m

clear all; clc; close all;

load('data.mat');

X = final_X;
y = final_y;

clear final_X final_y

% option1 = ['-v 10 -q -s 7 ', '-c ', num2str(c(i))];
% option2 = '-v 139  -s 7 ';
% option3 = '-s 0 -t 2  -q';

% impute NaN
for col = 1:size(X, 2)
    row = find(isnan(X(:, col)) == 1);
    X(row, col) = nanmean(X(:, col));
end

% normalization
X_mean = mean(X, 1);
X_std = nanstd(X);

for col = 1:size(X,2)
    if X_std(col) > 0
        X_norm(:, col) = (X(:, col) - repmat(X_mean(col), size(X, 1), 1))./repmat(X_std(col), size(X, 1), 1);
    else
        X_norm(:, col) = X(:, col) - repmat(X_mean(col), size(X, 1), 1);
    end
end


%% 10 fold cross validation

C = 4.^(-4:5);
g = 10.^(-4:5);

nC = length(C);
nG = length(g);

CVO = cvpartition(y, 'k', 10);

for nfeature = 2:100
    
    sel_index = feast('mim', nfeature , X_norm, y);
    X_sel = X_norm(:, sel_index);
    
    for c_index = 1:nC
        for g_index = 1:nG
            option = ['-q -s 0 -t 2 -c ', num2str(C(c_index)),' -g ', num2str(g(g_index))];
%             option = ['-q -s 0 -t 2 -c ', num2str(C(c_index))];

            for j = 1:CVO.NumTestSets
                index_train = CVO.training(j);
                index_test = CVO.test(j);
                X_train = X_sel(find(index_train == 1), :);
                X_test = X_sel(find(index_test == 1), :);
                y_train = y(find(index_train == 1));
                y_test = y(find(index_test == 1));
                
                model = svmtrain(y_train, X_train, option);
                [y_predict, accu(:,j), prob_predict] = svmpredict(y_test, X_test, model, '-q');
                
                %  y_predict
            end
            cv_accuracy_mean(c_index, g_index) = mean(accu(1, :));
            cv_accuracy_std(c_index, g_index) = std(accu(1, :));
        end
    end
    
    [max_cv_accu, index] = max(max(cv_accuracy_mean));
    [row, col] = find(cv_accuracy_mean == max_cv_accu, 1);
    C_best(nfeature) = C(row);
    g_best(nfeature) = g(col);
    
    accu_sel(nfeature) = max_cv_accu;
end

[max_accu_final, best_dim] = max(accu_sel);

%%%
plot(accu_sel(2:nfeature))

