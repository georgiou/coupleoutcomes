%Newly edited code to run classification (2 class) with static and dynamic functionals features
% link features

run configuration.m
run add_path.m

clear; clc; close all;

load('feat_link.mat');




final_y_link(find(final_y_link == 3 | final_y_link == 2 | final_y_link == 1 )) = 0;
final_y_link(find(final_y_link == 4)) = 1;

X = final_X_link;
y = final_y_link;

clear final_X_link final_y_link

% impute NaN
for col = 1:size(X, 2)
    row = find(isnan(X(:, col)) == 1);
    X(row, col) = nanmean(X(:, col));
end


% normalization
X_mean = mean(X, 1);
X_std = nanstd(X);

for col = 1:size(X,2)
    if X_std(col) > 0
        X_norm(:, col) = (X(:, col) - repmat(X_mean(col), size(X, 1), 1))./repmat(X_std(col), size(X, 1), 1);
    else
        X_norm(:, col) = X(:, col) - repmat(X_mean(col), size(X, 1), 1);
    end
end

%% PCA
[T, X_unc] = pca(X_norm);
X_unc = X_norm*T;


%% 10 fold cross validation

C = 4.^(-4:5);
g = 10.^(-4:5);

nC = length(C);
nG = length(g);

rng(1)
CVO = cvpartition(y, 'k', 10);

for nfeature = 5:100
    
    %sel_index = feast('mim', nfeature , X_norm, y);
    %sel_index=inn;
    %X_sel = X_norm(:, sel_index);
    
    X_sel = X_unc(:,1:nfeature);
    
    
    for c_index = 1:nC
        for g_index = 1:nG
            option = ['-q -s 0 -t 2 -c ', num2str(C(c_index)),' -g ', num2str(g(g_index))];
%             option = ['-q -s 0 -t 2 -c ', num2str(C(c_index))];

            for j = 1:CVO.NumTestSets
                index_train = CVO.training(j);
                index_test = CVO.test(j);
                X_train = X_sel(find(index_train == 1), :);
                X_test = X_sel(find(index_test == 1), :);
                y_train = y(find(index_train == 1));
                y_test = y(find(index_test == 1));
                
                model = svmtrain(y_train, X_train, option);
                [y_predict, accu(:,j), prob_predict] = svmpredict(y_test, X_test, model, '-q');
                
                %  y_predict
            end
            cv_accuracy_mean(c_index, g_index) = mean(accu(1, :));
            cv_accuracy_std(c_index, g_index) = std(accu(1, :));
        end
    end
    
    [max_cv_accu, index] = max(max(cv_accuracy_mean));
    [row, col] = find(cv_accuracy_mean == max_cv_accu, 1);
    C_best(nfeature) = C(row);
    g_best(nfeature) = g(col);
    
    accu_sel_mean(nfeature) = max_cv_accu
    accu_sel_std(nfeature) = cv_accuracy_std(row, col);
end

[max_accu_final, best_dim] = max(accu_sel_mean);
final_accu_std = accu_sel_std(best_dim);
%%%
plot(accu_sel_mean(2:nfeature))
max_accu_final
best_dim
final_accu_std
