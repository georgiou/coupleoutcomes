function [featD, N]=apply_dyn_func(feat_csv, feat_f0, feat_int, diar)


diar(:,4)=[];
diar(:,1:2)=diar(:,1:2)*100; 
diar(:,1)=diar(:,1)+1;
diar=round(diar);   % To indicate frame number


diarN=diar(end,2);     %last frame number
N=min([diarN size(feat_csv,1) size(feat_f0,1) size(feat_int,1)]);
%end correction
diar(diar(:,1:2)>N)=N;


%makes sure that 1=husband and 2=wife in diarization
diar=diar_id(diar,feat_f0);


feat_all=[feat_csv(1:N,3:end) feat_f0(1:N,:) feat_int(1:N,:) ];

cdiar=diar;
cdiar(find(cdiar(:,3)==0),:)=[];
%cdiar is diar matrix withour 0, continuos speech regions


feat_H=[];feat_W=[];hbuff=[];wbuff=[];

if cdiar(1,3)==1  %husband starts talking
    prev=2;
else
    prev=1;
end


     for i=1:size(cdiar,1)
         if cdiar(i,3)==1
             if prev==1
                hbuff=[hbuff; feat_all(cdiar(i,1):cdiar(i,2),:)];
             else %prev wife
                 hbuff=feat_all(cdiar(i,1):cdiar(i,2),:);
                 if ~isempty(wbuff)
                     feat_W=[feat_W; mean(wbuff,1)];
                 end
             end
             prev=1;
         else
             if prev == 2
                 wbuff = [wbuff; feat_all(cdiar(i,1):cdiar(i,2),:)];
             else
                 wbuff=feat_all(cdiar(i,1):cdiar(i,2),:);
                 if ~isempty(hbuff)
                    feat_H=[feat_H; mean(hbuff,1)];
                 end
             end
             prev=2;
         end      
     end
     
     
featHH= [feat_H; ones(1,size(feat_H,2))]-[ones(1,size(feat_H,2)); feat_H];
featWW= [feat_W; ones(1,size(feat_W,2))]-[ones(1,size(feat_W,2)); feat_W];
L=min([size(feat_H,1) size(feat_W,1)]);
% size(feat_H)
% size(feat_W)
featHW=feat_H(1:L,:)-feat_W(1:L,:);



feathh=compute_functional(featHH);
featww=compute_functional(featWW);
feathw=compute_functional(featHW);


featD=[feathh featww feathw];
if length(featD)==1367
    keyboard
end

end