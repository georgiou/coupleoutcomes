function [feat1, feat2, N]=apply_stat_func(feat_csv, feat_f0, feat_int, diar)
diar(:,4)=[];  %removes unnecessary part
diar(:,1:2)=diar(:,1:2)*100;   %matches time scale
diar(:,1)=diar(:,1)+1;         %starts with next frame instead of same
diar=round(diar);              % To indicate frame number
diarN=diar(end,2);     %last frame number
N=min([diarN size(feat_csv,1) size(feat_f0,1) size(feat_int,1)]);
diar(diar(:,1:2)>N)=N;  %end correction

%makes sure that 1=husband and 2=wife in diarization
diar=diar_id(diar,feat_f0);

feat_all=[feat_csv(1:N,3:end) feat_f0(1:N,:) feat_int(1:N,:) ];

feat1=[];
feat2=[];

%speaker 1
I1=find(diar(:,3)==1);
for i=1:size(I1,1)
    feat1=[feat1; feat_all(diar(I1(i),1):diar(I1(i),2),:)];   
end

%speaker 2
I2=find(diar(:,3)==2);
for i=1:size(I2,1)
    feat2=[feat2; feat_all(diar(I2(i),1):diar(I2(i),2),:)];   
end

feat1=compute_functional(feat1);
feat2=compute_functional(feat2);



end