#!/usr/bin/env python

import os
import subprocess
import glob




featLLD_dir='/home/nasir/Videos/data/couples/CoupTher_data_old/data/feat_LLD_couple/'

featProc_dir='/home/nasir/Videos/data/couples/CoupTher_data_old/data/feat_proc/'
featDAT_dir='/home/nasir/Videos/data/couples/CoupTher_data_old/data/feat_DAT/'
if not os.path.exists(featProc_dir):
	os.makedirs(featProc_dir)

if not os.path.exists(featDAT_dir):
	os.makedirs(featDAT_dir)

os.chdir(featLLD_dir)
for dir in os.listdir(featLLD_dir):
	if not os.path.exists(featProc_dir+dir):
		os.makedirs(featProc_dir+dir)
	os.chdir(dir)
	for file in glob.glob('*.f0'):
		os.system('sed 4d '+file+' > '+featProc_dir+dir+'/'+file+'.proc')
	for file in glob.glob('*.int'):
		os.system('sed 4d '+file+' > '+featProc_dir+dir+'/'+file+'.proc')
	for file in glob.glob('*.csv'):
		os.system('sed 1d '+file+' > '+featProc_dir+dir+'/'+file+'.proc')
	for file in glob.glob('*.dat'):
		os.system('mv '+file+' > '+featProc_dir+'/'+file)
	os.chdir('..')


	


