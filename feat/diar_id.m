function [diar]=diar_id(diar,feat_f0)
%makes sure that 1=husband and 2=wife in diarization

f1=[];
f2=[];

%speaker 1
I1=find(diar(:,3)==1);
for i=1:size(I1,1)
    f1=[f1; feat_f0(diar(I1(i),1):diar(I1(i),2),:)];
end

%speaker 2
I2=find(diar(:,3)==2);
for i=1:size(I2,1)
    f2=[f2; feat_f0(diar(I2(i),1):diar(I2(i),2),:)];
end

if mean(f1)>=mean(f2)
    diar(I1,3)=2*ones(size(I1,2),1);
    diar(I2,3)=1*ones(size(I1,2),1);
end
end