%% Script for combining Praat and opensmile raw fearues, apply diarization and compute functionals
clear; clc;

mainDir='/media/nasir/9e93c534-a1c6-4b3e-b5a3-16aaeb55ef96/couples/CoupTher_data_old/data/';
diarDir=[mainDir 'mdiar/'];
srcDir = '/home/nasir/Dropbox/outcomePrediction/src/feat/';

cd([mainDir 'feat_proc'])
list_couple=dir();
list_couple(1:2)=[];

Missing_pre=[];
Missing_26wk=[];
Missing_2yr=[];
Missing_both=[];
addpath(genpath(srcDir))

for i=4:size(list_couple)
    CoupleID=list_couple(i).name;
    cd(CoupleID)
    
    listF=dir();
    listF(1:2)=[];
    
    fileC=struct2cell(listF);
    fileC(2:5,:)=[];
    
    jj=strfind(fileC,'26wk');
    ll=strfind(fileC,'2yr');
    kk=strfind(fileC,'pre');
    if sum(sign(cell2mat(kk)))~=6    %number of pre-files
        Missing_pre=[Missing_pre; CoupleID];
        cd ..
        continue
    end
    
    flag26wk = 0;    
    if sum(sign(cell2mat(jj)))~=6
        Missing_26wk=[Missing_26wk; CoupleID];             %26wk is missing
        flag26wk = 1;
    end
    

    if sum(sign(cell2mat(ll)))~=6
        Missing_2yr=[Missing_2yr; CoupleID];
        %26wk is missing
    end
    
    if (sum(sign(cell2mat(jj)))~=6) && (sum(sign(cell2mat(ll)))~=6)
        Missing_both=[Missing_both; CoupleID];
        cd ..
        continue
    end
    
    
    
    if size(fileC,2)==18   %Cases where all sessions(it creates 6 files) present
        
        % pre.h
        feat_pre_h_csv=importdata(fileC{13});
        feat_pre_h_f0=importdata(fileC{14});
        feat_pre_h_int=importdata(fileC{15});
        file_root_name=fileC{13};
        file_root_name=file_root_name(1:13);
        diar=importdata([diarDir file_root_name 'diar']);
        [feat1, feat2]=apply_stat_func(feat_pre_h_csv, feat_pre_h_f0, feat_pre_h_int, diar);
        featD=apply_dyn_func(feat_pre_h_csv, feat_pre_h_f0, feat_pre_h_int, diar);
        s=struct('CoupleID',CoupleID,'Time','pre','Topic','h','feat1',feat1,'feat2',feat2,'featD',featD);
        save([mainDir 'feat_mat_new/' CoupleID '.pre.ps.h.mat'],'s');
        
        % pre.w
        feat_pre_w_csv=importdata(fileC{16});
        feat_pre_w_f0=importdata(fileC{17});
        feat_pre_w_int=importdata(fileC{18});
        file_root_name=fileC{16};
        file_root_name=file_root_name(1:13);
        diar=importdata([diarDir file_root_name 'diar']);
        [feat1, feat2]=apply_stat_func(feat_pre_w_csv, feat_pre_w_f0, feat_pre_w_int, diar);
        featD=apply_dyn_func(feat_pre_w_csv, feat_pre_w_f0, feat_pre_w_int, diar);
        s=struct('CoupleID',CoupleID,'Time','pre','Topic','w','feat1',feat1,'feat2',feat2,'featD',featD);
        save([mainDir 'feat_mat_new/' CoupleID '.pre.ps.w.mat'],'s');
        
        % 2yr.h
        feat_2yr_h_csv=importdata(fileC{7});
        feat_2yr_h_f0=importdata(fileC{8});
        feat_2yr_h_int=importdata(fileC{9});
        file_root_name=fileC{7};
        file_root_name=file_root_name(1:13);
        diar=importdata([diarDir file_root_name 'diar']);
        [feat1, feat2]=apply_stat_func(feat_2yr_h_csv, feat_2yr_h_f0, feat_2yr_h_int, diar);
        featD=apply_dyn_func(feat_2yr_h_csv, feat_2yr_h_f0, feat_2yr_h_int, diar);
        s=struct('CoupleID',CoupleID,'Time','2yr','Topic','h','feat1',feat1,'feat2',feat2,'featD',featD);
        save([mainDir 'feat_mat_new/' CoupleID '.2yr.ps.h.mat'],'s');
        
        % 2yr.w
        feat_2yr_w_csv=importdata(fileC{10});
        feat_2yr_w_f0=importdata(fileC{11});
        feat_2yr_w_int=importdata(fileC{12});
        file_root_name=fileC{10};
        file_root_name=file_root_name(1:13);
        diar=importdata([diarDir file_root_name 'diar']);
        [feat1, feat2]=apply_stat_func(feat_2yr_w_csv, feat_2yr_w_f0, feat_2yr_w_int, diar);
        featD=apply_dyn_func(feat_2yr_w_csv, feat_2yr_w_f0, feat_2yr_w_int, diar);
        s=struct('CoupleID',CoupleID,'Time','2yr','Topic','w','feat1',feat1,'feat2',feat2,'featD',featD);
        save([mainDir 'feat_mat_new/' CoupleID '.2yr.ps.w.mat'],'s');
        
        % 26wk.h
        feat_26wk_h_csv=importdata(fileC{1});
        feat_26wk_h_f0=importdata(fileC{2});
        feat_26wk_h_int=importdata(fileC{3});
        file_root_name=fileC{1};
        file_root_name=file_root_name(1:14);
        diar=importdata([diarDir file_root_name 'diar']);
        [feat1, feat2]=apply_stat_func(feat_26wk_h_csv, feat_26wk_h_f0, feat_26wk_h_int, diar);
        featD=apply_dyn_func(feat_26wk_h_csv, feat_26wk_h_f0, feat_26wk_h_int, diar);
        s=struct('CoupleID',CoupleID,'Time','26wk','Topic','h','feat1',feat1,'feat2',feat2,'featD',featD);
        save([mainDir 'feat_mat_new/' CoupleID '.26wk.ps.h.mat'],'s');
        
        % 26wk.w
        feat_26wk_w_csv=importdata(fileC{4});
        feat_26wk_w_f0=importdata(fileC{5});
        feat_26wk_w_int=importdata(fileC{6});
        file_root_name=fileC{4};
        file_root_name=file_root_name(1:14);
        diar=importdata([diarDir file_root_name 'diar']);
        [feat1, feat2]=apply_stat_func(feat_26wk_w_csv, feat_26wk_w_f0, feat_26wk_w_int, diar);
        featD=apply_dyn_func(feat_26wk_w_csv, feat_26wk_w_f0, feat_26wk_w_int, diar);
        s=struct('CoupleID',CoupleID,'Time','26wk','Topic','w','feat1',feat1,'feat2',feat2,'featD',featD);
        save([mainDir 'feat_mat_new/' CoupleID '.26wk.ps.w.mat'],'s');
        
    end
    
    
    if size(fileC,2)==12   %Cases where 2 sessions(it creates 4 files) present
        
        % pre.h
        feat_pre_h_csv=importdata(fileC{7});
        feat_pre_h_f0=importdata(fileC{8});
        feat_pre_h_int=importdata(fileC{9});
        file_root_name=fileC{7};
        file_root_name=file_root_name(1:13);
        diar=importdata([diarDir file_root_name 'diar']);
        [feat1, feat2]=apply_stat_func(feat_pre_h_csv, feat_pre_h_f0, feat_pre_h_int, diar);
        featD=apply_dyn_func(feat_pre_h_csv, feat_pre_h_f0, feat_pre_h_int, diar);
        s=struct('CoupleID',CoupleID,'Time','pre','Topic','h','feat1',feat1,'feat2',feat2,'featD',featD);
        save([mainDir 'feat_mat_new/' CoupleID '.pre.ps.h.mat'],'s');
        
        % pre.w
        feat_pre_w_csv=importdata(fileC{10});
        feat_pre_w_f0=importdata(fileC{11});
        feat_pre_w_int=importdata(fileC{12});
        file_root_name=fileC{10};
        file_root_name=file_root_name(1:13);
        diar=importdata([diarDir file_root_name 'diar']);
        [feat1, feat2]=apply_stat_func(feat_pre_w_csv, feat_pre_w_f0, feat_pre_w_int, diar);
        featD=apply_dyn_func(feat_pre_w_csv, feat_pre_w_f0, feat_pre_w_int, diar);
        s=struct('CoupleID',CoupleID,'Time','pre','Topic','w','feat1',feat1,'feat2',feat2,'featD',featD);
        save([mainDir 'feat_mat_new/' CoupleID '.pre.ps.w.mat'],'s');
        
        
        if flag26wk==1
            
            % 2yr.h
            feat_2yr_h_csv=importdata(fileC{1});
            feat_2yr_h_f0=importdata(fileC{2});
            feat_2yr_h_int=importdata(fileC{3});
            file_root_name=fileC{1};
            file_root_name=file_root_name(1:13);
            diar=importdata([diarDir file_root_name 'diar']);
            [feat1, feat2]=apply_stat_func(feat_2yr_h_csv, feat_2yr_h_f0, feat_2yr_h_int, diar);
            featD=apply_dyn_func(feat_2yr_h_csv, feat_2yr_h_f0, feat_2yr_h_int, diar);
            s=struct('CoupleID',CoupleID,'Time','2yr','Topic','h','feat1',feat1,'feat2',feat2,'featD',featD);
            save([mainDir 'feat_mat_new/' CoupleID '.2yr.ps.h.mat'],'s');
            
            
            % 2yr.w
            feat_2yr_w_csv=importdata(fileC{4});
            feat_2yr_w_f0=importdata(fileC{5});
            feat_2yr_w_int=importdata(fileC{6});
            file_root_name=fileC{4};
            file_root_name=file_root_name(1:13);
            diar=importdata([diarDir file_root_name 'diar']);
            [feat1, feat2]=apply_stat_func(feat_2yr_w_csv, feat_2yr_w_f0, feat_2yr_w_int, diar);
            featD=apply_dyn_func(feat_2yr_w_csv, feat_2yr_w_f0, feat_2yr_w_int, diar);
            s=struct('CoupleID',CoupleID,'Time','2yr','Topic','w','feat1',feat1,'feat2',feat2,'featD',featD);
            save([mainDir 'feat_mat_new/' CoupleID '.2yr.ps.w.mat'],'s');
            
            
        else
            
            
            
            % 26wk.h
            feat_26wk_h_csv=importdata(fileC{1});
            feat_26wk_h_f0=importdata(fileC{2});
            feat_26wk_h_int=importdata(fileC{3});
            file_root_name=fileC{1};
            file_root_name=file_root_name(1:14);
            diar=importdata([diarDir file_root_name 'diar']);
            [feat1, feat2]=apply_stat_func(feat_26wk_h_csv, feat_26wk_h_f0, feat_26wk_h_int, diar);
            featD=apply_dyn_func(feat_26wk_h_csv, feat_26wk_h_f0, feat_26wk_h_int, diar);
            s=struct('CoupleID',CoupleID,'Time','26wk','Topic','h','feat1',feat1,'feat2',feat2,'featD',featD);
            save([mainDir 'feat_mat_new/' CoupleID '.26wk.ps.h.mat'],'s');
            
            
            % 26wk.w
            feat_26wk_w_csv=importdata(fileC{4});
            feat_26wk_w_f0=importdata(fileC{5});
            feat_26wk_w_int=importdata(fileC{6});
            file_root_name=fileC{4};
            file_root_name=file_root_name(1:14);
            diar=importdata([diarDir file_root_name 'diar']);
            [feat1, feat2]=apply_stat_func(feat_26wk_w_csv, feat_26wk_w_f0, feat_26wk_w_int, diar);
            featD=apply_dyn_func(feat_26wk_w_csv, feat_26wk_w_f0, feat_26wk_w_int, diar);
            s=struct('CoupleID',CoupleID,'Time','26wk','Topic','w','feat1',feat1,'feat2',feat2,'featD',featD);
            save([mainDir 'feat_mat_new/' CoupleID '.26wk.ps.w.mat'],'s');
            
            
            
        end
    end
    
    
    cd ..
    
    
    fprintf('Couple %s processed, %d done\n \n', CoupleID,  i)
    
    
end





