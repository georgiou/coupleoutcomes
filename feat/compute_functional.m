function stats = compute_functional(x)
% compute functional of mfcc and mfb

% ignore f0 that are 0 (-999 after log norm) in a frame.

[nsample, nfeature] = size(x);




stats=[mean(x,1) median(x,1) std(x,1) min(x) max(x) range(x,1)];

f0=x(:,nfeature-1);

f0(find(f0==0))=[];

stats(1,nfeature-1)=mean(f0,1);
stats(1,2*nfeature-1)=median(f0,1);
stats(1,3*nfeature-1)=std(f0,1);
stats(1,4*nfeature-1)=min(f0);
stats(1,5*nfeature-1)=max(f0);
stats(1,6*nfeature-1)=range(f0,1);



if nsample<=1
    stats = repmat(x,1,6);
end




end