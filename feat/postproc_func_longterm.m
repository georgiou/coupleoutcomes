%%Srcipt for using feature files (sampled) to adapt to the classification

clear; clc; close all;

% 1 find pre and 26week, and pre, 2years, combine into one vector
% 2 match labels with couples, delete non-sense labels and features for couple

% data format:
% for both feature and label:
% 1 first part [pre, 26wk]
% 2 seocnd part [pre, 2yr]
% feature [s.feat1 s.feat2, s+1.feat1 s+1.feat2] h w respectively

% log:
% mar.16: if featD is > 1367, remove the left since the dimension of featD is not the same
addpath('..')
run import_outcome
path.feat = '~/Videos/data/couples/CoupTher_data_old/data/feat_mat_link_K16/';
f = dir('~/Videos/data/couples/CoupTher_data_old/data/feat_mat_link/*.samp');
nfile = length(f);

%% load label
label = reshape(outcomefinal(:,2:3),[],1);
label_coupleid = [outcomefinal(:,1);outcomefinal(:,1)];

nan_index = find(isnan(label) == 1);
label(nan_index) = [];
label_coupleid(nan_index) = [];

%remove repeated features (prc loudness and its de)
rem_id=[1 33 77 109];

%% load feature
% clear feature_pre coupleid_pre;
%keyboard;



count = 1;
for i = 1:2:nfile-1
    if strfind(f(i).name, 'pre')
        pre1 = load([path.feat, f(i).name]);
        pre1(:,rem_id)=[];
        pre2 = load([path.feat, f(i+1).name]);
        pre2(:,rem_id)=[];
        feature_pre(count,:) = reshape([pre1 pre2],1,[]);
        coupleid_pre(count) = str2num(strtok(f(i).name, '.'));
        count = count+1;
    end
end


% clear feature_26wk coupleid_26wk;
count = 1;
for i = 1:2:nfile-1
    if strfind(f(i).name, '26wk')
        pw1=load([path.feat, f(i).name]);
        pw1(:,rem_id)=[];
        pw2=load([path.feat, f(i+1).name]);
        pw2(:,rem_id)=[];

        feature_26wk(count,:) = reshape([pw1 pw2],1,[]);
        coupleid_26wk(count) = str2num(strtok(f(i).name, '.'));
        count = count+1;
    end
end


% clear feature_2yr coupleid_2yr;
count = 1;
for i = 1:2:nfile-1
    if strfind(f(i).name, '2yr')
        py1=load([path.feat, f(i).name]);
        py1(:,rem_id)=[];
        py2=load([path.feat, f(i+1).name]);
        py2(:,rem_id)=[];
        feature_2yr(count,:) = reshape([py1 py2],1,[]);
        coupleid_2yr(count) = str2num(strtok(f(i).name, '.'));
        count = count+1;
    end
end


%% combination of pre, 26wk and pre 2yr
coupleid_inter_pre_26 = intersect(coupleid_26wk, coupleid_pre);
coupleid_inter_pre_2yr = intersect(coupleid_2yr, coupleid_pre);

% clear feature
% pre and 26wk
for i = 1:length(coupleid_inter_pre_26)
    coupleid(i) = coupleid_inter_pre_26(i);
    index1 = find(coupleid_pre == coupleid_inter_pre_26(i));
    index2 = find(coupleid_26wk == coupleid_inter_pre_26(i));
    %feature(i,:) = [feature_pre(index1,:), feature_26wk(index2,:)]; %change
    feature(i,:) = -feature_pre(index1,:)+ feature_26wk(index2,:);
end

% pre and 2 yr
ninter_pre_26 = length(coupleid_inter_pre_26);
ninter_pre_2yr = length(coupleid_inter_pre_2yr);

for j = 1:ninter_pre_2yr
    coupleid(ninter_pre_26+j) = coupleid_inter_pre_2yr(j);
    index1 = find(coupleid_pre == coupleid_inter_pre_2yr(j));
    index2 = find(coupleid_2yr == coupleid_inter_pre_2yr(j));
    %feature(ninter_pre_26+j,:) = [feature_pre(index1,:), feature_2yr(index2,:)];
    feature(ninter_pre_26+j,:) = -feature_pre(index1,:) + feature_2yr(index2,:);
end

%% match label and feature
% inter_feature_label = intersect(coupleid,label_coupleid);

coupleid_26wk = coupleid(1:find(coupleid == 662, 1, 'first'));
coupleid_2yr = coupleid(find(coupleid == 662, 1, 'first')+1 : end);

label_coupleid_26wk = label_coupleid(1:find(label_coupleid == 662, 1, 'first'));
label_coupleid_2yr = label_coupleid(find(label_coupleid == 662, 1, 'first')+1 : end);

inter_feature_label_26wk = intersect(coupleid_26wk, label_coupleid_26wk);
inter_feature_label_2yr = intersect(coupleid_2yr, label_coupleid_2yr);


for m = 1:length(coupleid_26wk)
    index = find(label_coupleid == coupleid(m), 1, 'first');
    final_y(m) = label(index);
end

for n = 1:length(coupleid_2yr)
    index = find(label_coupleid == coupleid(length(coupleid_26wk)+n), 1, 'last');
    final_y(length(coupleid_26wk)+n) = label(index);
end

final_X = feature;
final_y = final_y';
final_coupleid = coupleid;

