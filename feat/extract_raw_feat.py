#!/usr/bin/env python

# Script written by Md Nasir
# This script is a wrapper for running the perl script MAIN_extract_LLD for all the audio files
#low level features are stored in feat_dir in the same structure as the audio_dir

import os
import subprocess
import glob

script_dir='/home/nasir/Dropbox/outcomePrediction/src/feat/'
audio_dir='/home27/couples/CoupTher_data_old/data/audio/'
audio_dir_ds='/home27/couples/CoupTher_data_old/data/audio_ds_new/'
feat_dir='/home27/couples/CoupTher_data_old/data/feat_LLD_new/'

os.chdir(audio_dir_ds)
for dir in os.listdir(audio_dir):
	if not os.path.exists(dir):
    		os.makedirs(dir)

os.chdir(feat_dir)

for dir in os.listdir(audio_dir):
	if not os.path.exists(dir):
		os.makedirs(dir)
	os.chdir(script_dir)
	os.system('perl MAIN_extract_LLD.pl'+' '+audio_dir+dir+' '+audio_dir_ds+dir+' '+feat_dir+dir)
	os.chdir(feat_dir)
