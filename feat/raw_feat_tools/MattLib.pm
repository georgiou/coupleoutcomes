#
#===============================================================================
#
#         FILE:  MattLib.pm
#
#  DESCRIPTION:  This is a collection of various handy utilities for 
#                 extracting features for the Couples Therapy corpus
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Matthew P. Black (MPB), matthepb@usc.edu
#      COMPANY:  University of Southern California
#      VERSION:  1.0
#      CREATED:  January 21, 2011
#     REVISION:  ---
#===============================================================================
use strict;
use warnings;
use File::Spec::Functions;


# Description:
#  This function creates a Praat f0 script for a single input audio file
#
# Inputs:
#  $audio_in - input audio filename
#  $f0_out - the output f0 filename
#  $script_praat - the filename of the Praat script
#  $min_f0 - the minimum f0 (pitch) value
#  $max_f0 - the maximum f0 (pitch) value
#
# Output:
#  [none]
#
# Examples:
#
sub make_praat_f0_script {

    # read in the inputs
    my ($audio_in, $f0_out, $script_praat, $min_f0, $max_f0) = @_;

    # check if input file and output directory exist
    die "Input audio file does not exist: $audio_in\n" unless (-e $audio_in);

    # print to Praat script file
    open(OUT,">$script_praat");
    print OUT 'infile$ = '."\"$audio_in\"\n";
    print OUT 'outfile$ = '."\"$f0_out\"\n";
    print OUT q(Read from file... 'infile$')."\n";
    print OUT 'object_name$ = selected$("Sound")'."\n";
    print OUT 'To Pitch (ac)... 0.01 '.$min_f0.' 15 no 0.03 0.45 0.01 0.35 0.14 '.$max_f0."\n";
    print OUT 'Smooth... 10'."\n";
    print OUT 'To Matrix'."\n";
    print OUT 'Transpose'."\n";
    print OUT q(Write to matrix text file... 'outfile$')."\n";
    print OUT 'Remove'."\n";
    print OUT q(select Matrix 'object_name$')."\n";
    print OUT 'Remove'."\n";
    print OUT q(select Pitch 'object_name$')."\n";
    print OUT 'Remove'."\n";
    print OUT q(select Sound 'object_name$')."\n";
    print OUT 'Remove'."\n";
    close(OUT);
}


# Description:
#  This function creates a Praat intensity script for a single input audio file
#
# Inputs:
#  $audio_in - input audio filename
#  $int_out - the output intensity filename
#  $script_praat - the filename of the Praat script
#  $min_f0 - the minimum f0 (pitch) value
#
# Output:
#  [none]
#
# Examples:
#
sub make_praat_int_script {

    # read in the inputs
    my ($audio_in, $int_out, $script_praat, $min_f0) = @_;

    # check if input file and output directory exist
    die "Input audio file does not exist: $audio_in\n" unless (-e $audio_in);

    # print to Praat script file
    open(OUT,">$script_praat");
    print OUT 'infile$ = '."\"$audio_in\"\n";
    print OUT 'outfile$ = '."\"$int_out\"\n";
    print OUT q(Read from file... 'infile$')."\n";
    print OUT 'object_name$ = selected$("Sound")'."\n";
    print OUT 'To Intensity... '.$min_f0.' 0.01 no'."\n";
    print OUT 'Down to Matrix'."\n";
    print OUT 'Transpose'."\n";
    print OUT q(Write to matrix text file... 'outfile$')."\n";
    print OUT 'Remove'."\n";
    print OUT q(select Matrix 'object_name$')."\n";
    print OUT 'Remove'."\n";
    print OUT q(select Intensity 'object_name$')."\n";
    print OUT 'Remove'."\n";
    print OUT q(select Sound 'object_name$')."\n";
    print OUT 'Remove'."\n";
    close(OUT);
}


# Description:
#  This function defines an output filename
# 
# Inputs:
#  $filename_in - the input filename (written for the Couples Therapy corpus)
#  $dir_out - the output directory
#  $suffix_out - the output filename's suffix
#
# Output:
#  $filename_out - the output filename
#
# Examples: 
#  make_filename('../data/audio/521/521.2yr.ps.h.wav', '../feats/opensmile/', '.csv');
#   will return '../feats/opensmile/521.2yr.ps.h.csv'
#  make_filename('../data/audio/211/211.2yr.ps.h_tape.wav', '../feats/opensmile/', '.csv');
#   will return '../feats/opensmile/211.2yr.ps.h.csv'
#
sub make_filename {

    # read in the inputs
    my ($filename_in, $dir_out, $suffix_out) = @_;
    
    # check if input file and output directory exist
    die "Input file does not exist: $filename_in\n" unless (-e $filename_in);
    die mkdir $dir_out unless (-d $dir_out);

    # strip session name from input filename
    my @filename = split(/\//,$filename_in);
    my $session_name = $filename[-1];
    
    # remove the string "_tape" from session name (if it exists)
    $session_name =~ s/\_tape//;
    
    # remove the existing suffix and replace with the specified suffix
    $session_name =~ s/\.[a-zA-Z\d]+$//;

    # remove any periods in "suffix_out" (some users might input it with a period, some without)
    $suffix_out =~ s/\.//g;

    # remove trailing '/' in output directory (some users might input it with one, some without)
    $dir_out =~ s/[\/]+$//;

    # define output filename
    my $filename_out = $dir_out . '/' . $session_name . '.' . $suffix_out;

    # return output filename
    return ($filename_out);
}


# Description:
#  This function calls a system function and displays standard messages to the screen
#
# Input:
#  $command - the system command
#
# Output:
#  [none]
#
# Example:
#  run_system_command("ls");
#   will return a header, the system command, any results from the system command, and a footer
#
sub run_system_command {

    # read in input
    my ($command) = @_;

    # define header and footer messages
    my $header = "---> running system command:\n$command";
    my $footer = "<--- system command complete";

    # print header to screen
    print "$header\n";

    # run command
    system("$command");

    # print footer to screen
    print "$footer\n\n";

    # return system return message
    ### nothing is returned
}


1;
__END__
