#!/usr/bin/perl
#===============================================================================
#
#         FILE:  MAIN_extract_LLD.pl
#
#        USAGE:  ./MAIN_extract_LLD.pl
#
#  DESCRIPTION:  Main file to extract the raw acoustic low-level descriptors
#                 (LLDs) for the Interspeech 2015 Challenge using Praat and openSMILE
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Matthew P. Black, Ph.D., mblack@isi.edu
#      COMPANY:  Information Sciences Institute, University of Southern California
#      VERSION:  1.1
#      CREATED:  January 21, 2011
#     REVISION:  January 27, 2015
#===============================================================================
use strict;
use warnings;
use File::Spec::Functions;
use MattLib;

###
### Inputs
###


#pass the directories to this script via system call in the python script

my ($dir_audio, $dir_audio_down, $dir_feats) = @ARGV;




# input directory
#my $dir_audio = '../../data/is2015_challenge_eating/wav/'; # input directory of audio files (*.wav)
#my $dir_audio = '../../data/is2015_challenge_Nativeness/wav/';
#my $dir_audio = '../../wav/';
#my $dir_audio_down = '../../wav_16kHz/';

# output directories
#my $dir_feats = '../../feats/LLD/is2015_challenge_eating/';  # output directory
#my $dir_feats = '../../feats/LLD/is2015_challenge_Nativeness/';
#my $dir_feats = '../../LLD_Nasir/';

# configuration files for openSMILE
my $config_opensmile_csv = './emobase2010_specom_csv.conf'; # output to *.csv files
my $config_opensmile_dat = './emobase2010_specom_dat.conf'; # output to *.dat files

# files for the SAIL VAD
#my $vad_bin = '../vad/a.out';
#my $vad_mod = '../vad/model_0.3_0.1_350_3500_0.9_1e-09_10000_2048.bin';
#my $tmp_audio = '../tmp/audio.wav';
#my $tmp_audio2 = '../tmp/audio2.wav';

# temporary script file for Praat
my $script_praat = './praat.script'; # temporary file that is input to praat

# filename suffixes
my $suffix_audio_in = '.wav'; # input audio files
#my $suffix_vad = '.vad'; # output VAD files
#my $suffix_ltsv = '.ltsv'; # output VAD files with LTSV information
my $suffix_praat_f0 = '.f0'; # output praat f0 files
my $suffix_praat_int = '.int'; # output praat intensity files
my $suffix_opensmile_csv = '.csv'; # output openSMILE CSV files
my $suffix_opensmile_dat = '.dat'; # output openSMILE "dat" files

# parameters for praat f0 estimation
my $min_f0 = 70;
my $max_f0 = 400;
my $min_f0_int = 70; # minimum f0 needed for intensity estimation

###
### MAIN
###
# Parse input directory for audio files
open(FIND, "find $dir_audio -print |") || die "Couldn't run find: $!\n";
FILE:
    while (my $audio_in = <FIND>){
        chomp($audio_in);
        next FILE unless ($audio_in =~ /$suffix_audio_in$/);

	###
	### DOWNSAMPLE PARKINSON'S AUDIO TO 16KHZ (unless file already downsampled)
	###
	if ($audio_in =~ /Parkinson/){
	    # Specify downsampled audio file
	    my $audio_down = $audio_in;
	    $audio_down =~ s/^$dir_audio/$dir_audio_down/;
	    unless (-e $audio_down){
		my $cmd = "sox $audio_in $audio_down rate 16000 dither -s";
		print "$cmd\n";
		system($cmd);
	    }
	    # replace variable with downsampled audio for compatibility with remainder of code
	    $audio_in = $audio_down;
	}

#        ### extract VAD features
#        my $vad_out = make_filename($audio_in, $dir_feats, $suffix_vad);
#	my $ltsv_out = make_filename($audio_in, $dir_feats, $suffix_ltsv);
#        unless (-e $vad_out){
#	    # convert audio to 16kHz and 16bit
#	    my $cmd = "sox $audio_in -r 16000 -b 16 $tmp_audio2";
#	    system("$cmd");
#	    # normalize max abs sample value 
#	    my $vol_adj= `sox $tmp_audio2 -n stat -v 2>&1` + 0;
#	    $cmd = "sox -v $vol_adj $tmp_audio2 $tmp_audio";
#	    system("$cmd");
#           run_system_command("$vad_bin -i $tmp_audio -m $vad_mod -o $vad_out --dont-print-frame-number --print-timing");
#	    unless (-e $ltsv_out){
#		run_system_command("$vad_bin -i $tmp_audio -m $vad_mod -o $ltsv_out --print-LTSV");
#	    }
#	}

	### extract Praat f0 (pitch) features
	# define the filenames for the praat f0 output files (add pitch range to output directory name)
	my $f0_out = make_filename($audio_in, $dir_feats, $suffix_praat_f0);
	unless (-e $f0_out){ # do not call Praat if output file already exists
	    # call function that makes Praat f0 script file
	    make_praat_f0_script($audio_in, $f0_out, $script_praat, $min_f0, $max_f0);

###	    print "$audio_in\n$f0_out\n";
	    
	    # extract f0 features by calling Praat with Praat script file
	    run_system_command("praat $script_praat");
	}

	### extract Praat intensity features
	my $int_out = make_filename($audio_in, $dir_feats, $suffix_praat_int);
	# call function that makes intensity Praat script file and stores it in a tmp directory
	unless (-e $int_out){ # do not call Praat if output file already exists
	    # call function that makes Praat intensity script file
	    make_praat_int_script($audio_in, $int_out, $script_praat, $min_f0_int);

###	    print "$int_out\n";

	    # extract intensity features by calling Praat with Praat script file
	    run_system_command("praat $script_praat");
	}

        ### extract openSMILE features
        # and save to *.csv file that contains the feature names (comma-delimited text files)
	my $csv_out = make_filename($audio_in, $dir_feats, $suffix_opensmile_csv);
	unless (-e $csv_out){ # do not call openSMILE if output file already exists

###	    print "$csv_out\n";

#	    run_system_command("SMILExtract -nologfile -C $config_opensmile_csv -I $audio_in -O $csv_out");
	    run_system_command("SMILExtract -nologfile -C $config_opensmile_csv -l 0 -I $audio_in -O $csv_out");
            # -l 0 (for no output stuff)
	}

        # and save to *.dat (binary) file, that have small file sizes and can be read by Matlab
	my $dat_out = make_filename($audio_in, $dir_feats, $suffix_opensmile_dat);
	unless (-e $dat_out){ # do not call openSMILE if output file already exists
	    
###	    print "$dat_out\n\n";

#	    run_system_command("SMILExtract -nologfile -C $config_opensmile_dat -I $audio_in -O $dat_out");
	    run_system_command("SMILExtract -nologfile -C $config_opensmile_dat -l 0 -I $audio_in -O $dat_out");
	    # -l 0 (for no output stuff)
	}
}
close(FIND);
